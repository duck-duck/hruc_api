<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view('welcome');
});

Route::post('locale','LanguageController@change');

Route::group(['middleware'=>['locale','bindings'],'prefix'=>'auth', 'namespace'=>'Auth'], function() {
    Route::post('register/email', 'RegisterController@registerByEmail');
    Route::post('password/forgot', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
    Route::post('verify/email', 'VerificationController@verifyEmail');
    Route::get('user/{client}/resend-vrification-email', 'VerificationController@resend');
    Route::post('login', 'LoginController@login')->middleware('throttle:5,1');

    Route::group(['middleware' => 'auth:client'], function ($router) {
        Route::post('logout', 'LoginController@logout');
        Route::post('refresh', 'LoginController@refresh');
        Route::post('me', 'LoginController@me');
    });
});

Route::group([
    'middleware' => ['auth:client','locale']
], function () {

});
