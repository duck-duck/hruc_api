<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
* Auth routes
**/

Route::post('auth/login', 'Auth\AuthController@login')
    ->middleware(['cors','throttle:5,1']);

Route::group([
    'middleware' => ['auth:admin','cors','bindings','locale']
], function () {
    Route::post('auth/logout', 'Auth\AuthController@logout');
    Route::post('auth/refresh', 'Auth\AuthController@refresh');
    Route::post('auth/me', 'Auth\AuthController@me');

    Route::get('category/list', 'CategoryController@list');
    Route::get('material/{material}/remove-image', 'MaterialController@removeImage');
    Route::post('product/upload-image', 'ProductController@uploadImage');
    Route::post('product/upload-images', 'ProductController@uploadImages');
    Route::get('product/{product}/remove-image/{image?}', 'ProductController@removeImage');
    Route::patch('order/{order}/status', 'OrderController@changeStatus');
    Route::get('order/status-list', 'OrderController@statusList');
    Route::delete('order/{order}/item/{order_item}', 'OrderController@deleteOrderItem');

    Route::apiResource('brand', 'BrandController');
    Route::apiResource('camera', 'CameraController');
    Route::apiResource('lenses', 'LensController');
    Route::apiResource('material', 'MaterialController');
    Route::apiResource('category', 'CategoryController');
    Route::apiResource('product', 'ProductController');
    Route::apiResource('order', 'OrderController')->only(['index','update','show','destroy']);
});
