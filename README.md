# HruCase API

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

Environment.

  - Laravel Framework 6.12.0
  - PHP >= 7.2.0
  - mysql:5.7

Install ...

```sh
$ composer install
$ php artisan jwt:secret
$ php artisan key:generate
$ php artisan storage:link
$ php artisan migrate
$ php artisan db:seed
$ php artisan db:seed --class=LaratrustSeeder
```
