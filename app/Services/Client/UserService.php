<?php

namespace App\Services\Client;

use App\Services\Service;
use App\Models\Client;
use App\Models\Laratrust\Role;
use Hash, DB, Log;

class UserService extends Service
{

    public function __construct()
    {
        //
    }

    /**
     * Store Client
     *
     * @param array $data
     * @return App\Models\Client
     * @throws Exception
     */
    public function registerByEmail($data) : Client
    {
        try {
            DB::beginTransaction();
            $user = Client::create($data);
            $user->setEmail($data['email']);
            $user->setPassword(Hash::make($data['password']));
            $user->setConfirmationCode();
            $user->attachRole(Role::CLIENT);

            DB::commit();
        } catch (Exception $exception) {
            return $this->handleTransactionException($exception);
        }

        return $user;
    }
}
