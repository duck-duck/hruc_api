<?php

namespace App\Services\Admin;

use App\Services\Service;
use App\Models\Lense;
use DB;
use Log;

class LensService extends Service
{
    /**
     * Store Lense
     *
     * @param array $data
     * @return App\Models\Lense
     * @throws Exception
     */
    public function create($data) : Lense
    {
        try {
            DB::beginTransaction();

            $lense = Lense::create($data);

            if (!empty($data['brand_id'])) {
                $lense->brand()->associate($data['brand_id'])->save();
            }

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $lense;
    }

    /**
     * Update Lens
     *
     * @param Lense $lense
     * @param array $data
     * @return App\Models\Lense
     * @throws Exception
     */
    public function update(Lense $lense, $data) : Lense
    {
        try {
            DB::beginTransaction();

            if (!empty($data['brand_id'])) {
                $lense->brand()->associate($data['brand_id'])->save();
            }

            $lense->update($data);

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $lense;
    }

    /**
     * Delete Lense
     * @param  Lense $lense
     * @return bool
     */
    public function delete(Lense $lense) : bool
    {
        return $lense->delete();
    }
}
