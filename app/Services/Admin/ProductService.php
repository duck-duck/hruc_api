<?php

namespace App\Services\Admin;

use App\Services\Service;
use App\Models\Product;
use Illuminate\Http\UploadedFile;
use DB, Log, Image, Str, Storage;
use App\Models\Image as ModelImage;

class ProductService extends Service
{
    public function __construct()
    {
        //
    }

    /**
     * Store Product
     *
     * @param array $data
     *
     * @return App\Models\Product
     * @throws Exception
     */
    public function create($data) : Product
    {
        try {
            DB::beginTransaction();
            $product = Product::create($data);

            $product->category()->associate($data['category_id'])->save();

            if (!empty($data['images'])) {
                $this->syncImages($product, $data['images']);
            }

            $product->materials()->sync($data['materials']);
            $product->cameras()->sync($data['cameras']);
            $product->lenses()->sync($data['lenses']);
            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $product;
    }

    /**
     * Update Lens
     *
     * @param Product $product
     * @param array $data
     * @return App\Models\Product
     * @throws Exception
     */
    public function update(Product $product, $data) : Product
    {
        try {
            DB::beginTransaction();

            $product->update($data);
            $product->category()->associate($data['category_id'])->save();

            if (!empty($data['images'])) {
                $this->syncImages($product, $data['images']);
            }

            $product->materials()->sync($data['materials']);
            $product->cameras()->sync($data['cameras']);
            $product->lenses()->sync($data['lenses']);
            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $product;
    }

    public function uploadImage(UploadedFile $uploadedFile)
    {
        try {
            $fileName = strtolower(Str::random(12)) . '.' . $uploadedFile->guessClientExtension();
            $imageInstance = Image::make($uploadedFile);

            if ($imageInstance->height() > config('image.product.height')) {
                $imageInstance->resize(config('image.product.height'), null, function ($cons) {
                    $cons->aspectRatio();
                    $cons->upsize();
                });
            }

            Storage::put(Product::getImagePath($fileName), $imageInstance->stream()->__toString());
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $fileName;
    }

    /**
     * Sync Product images
     * @param  Product $product
     * @param array|null $images
     * @return void
     */
    public function syncImages(Product $product, $images)
    {
        if (empty($images)) {
            return;
        }

        foreach ($images as $image) {
            $product->images()->create(['name'=>$image]);
        }
    }

    /**
     * Delete Product
     * @param  Product $product
     * @return bool
     */
    public function delete(Product $product) : bool
    {
        $this->removeImage($product);
        return $product->delete();
    }

    /**
     *
     * @param  Product    $product
     * @param  ModelImage $image
     * @return boolean
     */
    public function removeImage(Product $product, ModelImage $image) : bool
    {
        $image_name = ($image->name) ? $image->name : $product->image;
        $result = Storage::delete($product->getImagePath($image_name));

        if ($image->name) {
            $result = $image->delete();
        } else {
            $product->image = null;
            $result = $product->save();
        }

        return $result;

    }
}
