<?php

namespace App\Services\Admin;

use App\Services\Service;
use App\Models\Camera;
use DB;
use Log;

class CemeraService extends Service
{
    /**
     * Store Camera
     *
     * @param array $data
     * @return App\Models\Camera
     * @throws Exception
     */
    public function create($data) : Camera
    {
        try {
            DB::beginTransaction();

            $camera = Camera::create($data);

            if (!empty($data['brand_id'])) {
                $camera->brand()->associate($data['brand_id'])->save();
            }

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $camera;
    }

    /**
     * Update Camera
     *
     * @param Camera $camera
     * @param array $data
     * @return App\Models\Camera
     * @throws Exception
     */
    public function update(Camera $camera, $data) : Camera
    {
        try {
            DB::beginTransaction();

            if (!empty($data['brand_id'])) {
                $camera->brand()->associate($data['brand_id'])->save();
            }

            $camera->update($data);

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $camera;
    }

    /**
     * Delete Camera
     * @param  Camera $camera
     * @return bool
     */
    public function delete(Camera $camera) : bool
    {
        return $camera->delete();
    }
}
