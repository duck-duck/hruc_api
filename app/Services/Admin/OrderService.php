<?php

namespace App\Services\Admin;

use App\Services\Service;
use App\Models\OrderItem;
use App\Models\Order;
use DB;
use Log;

class OrderService extends Service
{
    /**
     * Update Order
     *
     * @param Order $order
     * @param array $data
     * @return App\Models\Order
     * @throws Exception
     */
    public function update(Order $order, $data) : Order
    {
        try {
            DB::beginTransaction();

            if (!empty($data['status_id'])) {
                $order->status()->associate($data['status_id'])->save();
            }

            $order->update($data);

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $order;
    }

    /**
     * Delete Order
     * @param  Order $order
     * @return bool
     */
    public function delete(Order $order) : bool
    {
        return $order->delete();
    }

    /**
     * Delete OrderItem
     * @param  OrderItem $orderItem
     * @return bool
     */
    public function deleteOrderItem(OrderItem $orderItem) : bool
    {
        try {
            DB::beginTransaction();
            $orderItem->order->decrement('total',$orderItem->price);
            $status = $orderItem->delete();
            DB::commit();
        } catch (\Exception $e) {
            $this->handleException($exception);
        }

        return $status;
    }
}
