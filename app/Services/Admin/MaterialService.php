<?php

namespace App\Services\Admin;

use App\Services\Service;
use App\Models\Material;
use Illuminate\Support\Facades\Storage;
use App\Traits\ImageUpload;
use DB;
use Log;

class MaterialService extends Service
{
    use ImageUpload;

    public function __construct()
    {
        //
    }

    /**
     * Store Material
     *
     * @param FormRequest $request
     *
     * @return App\Models\Material
     * @throws Exception
     */
    public function create($request) : Material
    {
        try {
            DB::beginTransaction();

            $material = Material::create($request->all());
            $this->saveImage($material, $request->image);

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $material;
    }

    /**
     * Update Lens
     *
     * @param Material $material
     * @param FormRequest $request
     * @return App\Models\Material
     * @throws Exception
     */
    public function update(Material $material, $request) : Material
    {
        try {
            DB::beginTransaction();

            $material->update($request->all());
            $this->saveImage($material, $request->image);

            DB::commit();
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $material;
    }

    /**
     * Delete Material
     * @param  Material $material
     * @return bool
     */
    public function delete(Material $material) : bool
    {
        $this->removeImage($material);
        return $material->delete();
    }

    /**
     * Process Image File
     * @param  Material $material
     * @param  UploadedFile | null $file
     * @return void
     */
    private function saveImage(Material $material, $image) : void
    {
        if ($image) {
            $fileName = $this->storeSingleFile($material, $image, $crop = true);
            $material->image = $fileName;
            $material->save();
        }
    }

    public function removeImage(Material $material)
    {
        $result = Storage::delete($material->getImagePath().'/'.$material->image);
        if ($result) {
            $material->image = null;
            $material->save();
        }

        return $result;

    }
}
