<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Image;
use DB;
use Log;

trait ImageUpload
{
    /**
    * @param Model $model
    * @param UploadedFile $file
    * @param boolean $crop
    * @param array $size
    *
    * @return string $fileName
    **/
    public function storeSingleFile(
        Model $model,
        UploadedFile $file,
        $crop = false,
        $size = ['x'=>500, 'y'=>500]
        ) : string
    {
        $fileName = Str::kebab($model->name) . "." . $file->getClientOriginalExtension();
        // store without resizing
        //$file->storeAs($model->getImagePath(), $fileName);

        // Store resized image
        $resize_image = Image::make($file->getRealPath());

        if ($crop && ($resize_image->width() > $size['y'] || $size['x'] > $resize_image->height())) {
            $resize_image->crop($size['x'], $size['y']);
        }

        $resize_image->resize($size['x'], $size['y'], function($constraint){
            $constraint->aspectRatio();
        });

        $resize_image->save(storage_path('app/public/'.$model->getImagePath()).'/'.$fileName);

       return $fileName;
    }
}
