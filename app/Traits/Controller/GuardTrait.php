<?php

namespace App\Traits\Controller;

use Auth;

trait GuardTrait
{
    public function __construct()
    {
        Auth::shouldUse($this->guard);
    }

    /**
     * Gets the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard($this->guard);
    }
}
