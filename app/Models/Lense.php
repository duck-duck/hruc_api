<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;

class Lense extends Model
{
    use Filterable;

    protected $fillable = [
        'name',
        'width',
        'depth'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'width' => 'integer',
        'depth' => 'integer',
    ];

    /**
     * Get the brand of the lens.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * The products that belong to the lense.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class,'product_lense');
    }
}
