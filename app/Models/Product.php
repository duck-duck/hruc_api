<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasTranslations;

    public $translatable = ['description'];
    protected $fillable = [
        'name',
        'quantity',
        'description',
        'status',
        'slug',
        'price',
        'image'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean'
    ];

    public function getStoragePath()
    {
        return strtolower(class_basename($this));
    }

    /**
     * Get all of the post's comments.
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * The materials that belong to the product.
     */
    public function materials()
    {
        return $this->belongsToMany(Material::class,'product_material');
    }

    /**
     * The lenses that belong to the product.
     */
    public function lenses()
    {
        return $this->belongsToMany(Lense::class,'product_lense');
    }

    /**
     * The cameras that belong to the product.
     */
    public function cameras()
    {
        return $this->belongsToMany(Camera::class, 'product_camera');
    }

    /**
     * The category that the product belongs.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Scope a query to only include active products.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    /**
    *
    * @param string $image_name
    * @param boolean $full false - relative| true - full url
    *
    **/
    public function getImagePath(string $image_name, $full = false)
    {
        return $full ? url('storage/'.$this->getStoragePath().'/'.$image_name) : $this->getStoragePath().'/'.$image_name;
    }
}
