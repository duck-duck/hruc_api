<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;

class Camera extends Model
{
    use Filterable;

    protected $fillable = [
        'name',
        'width',
        'height',
        'depth'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'width' => 'integer',
        'height' => 'integer',
        'depth' => 'integer',
    ];

    /**
     * Get the brand of the camera.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * The products that belong to the camera.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class,'product_camera');
    }
}
