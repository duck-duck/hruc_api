<?php

namespace App\Models\Policies;

use App\Models\Order;
use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any Order.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        return $user->canReadOrder();
    }

    /**
     * Determine whether the user can view the Order.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function view(Admin $user, Order $order)
    {
        return $user->canReadOrder();
    }

    /**
     * Determine whether the user can create Order.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->canCreateOrder();
    }

    /**
     * Determine whether the user can update the Order.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function update(Admin $user, Order $order)
    {
        return $user->canUpdateOrder();
    }

    /**
     * Determine whether the user can delete the Order.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function delete(Admin $user, Order $order)
    {
        return $user->canDeleteOrder();
    }

    /**
     * Determine whether the user can restore the Order.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function restore(Admin $user, Order $order)
    {
        $user->canCreateOrder();
    }

    /**
     * Determine whether the user can permanently delete the Order.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Order  $order
     * @return mixed
     */
    public function forceDelete(Admin $user, Order $order)
    {
        $user->canDeleteOrder();
    }
}
