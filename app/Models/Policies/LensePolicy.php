<?php

namespace App\Models\Policies;

use App\Models\Lense;
use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class LensePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any Lens.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        return $user->canReadLens();
    }

    /**
     * Determine whether the user can view the Lens.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Lense  $lense
     * @return mixed
     */
    public function view(Admin $user, Lense $lense)
    {
        return $user->canReadLens();
    }

    /**
     * Determine whether the user can create Lens.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->canCreateLens();
    }

    /**
     * Determine whether the user can update the Lens.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Lense  $lense
     * @return mixed
     */
    public function update(Admin $user, Lense $lense)
    {   return true;
        return $user->canUpdateLens();
    }

    /**
     * Determine whether the user can delete the Lens.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Lense  $lense
     * @return mixed
     */
    public function delete(Admin $user, Lense $lense)
    {
        return $user->canDeleteLens();
    }

    /**
     * Determine whether the user can restore the Lens.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Lense  $lense
     * @return mixed
     */
    public function restore(Admin $user, Lense $lense)
    {
        $user->canCreateLens();
    }

    /**
     * Determine whether the user can permanently delete the Lens.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Lense  $lense
     * @return mixed
     */
    public function forceDelete(Admin $user, Lens $lense)
    {
        $user->canCreateLens();
    }
}
