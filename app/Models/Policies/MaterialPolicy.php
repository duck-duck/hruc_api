<?php

namespace App\Models\Policies;

use App\Models\Material;
use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class MaterialPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any Material.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        return $user->canReadMaterial();
    }

    /**
     * Determine whether the user can view the Material.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Material  $material
     * @return mixed
     */
    public function view(Admin $user, Material $material)
    {
        return $user->canReadMaterial();
    }

    /**
     * Determine whether the user can create Material.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->canCreateMaterial();
    }

    /**
     * Determine whether the user can update the Material.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Material  $material
     * @return mixed
     */
    public function update(Admin $user, Material $material)
    {
        return $user->canUpdateMaterial();
    }

    /**
     * Determine whether the user can delete the Material.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Material  $material
     * @return mixed
     */
    public function delete(Admin $user, Material $material)
    {
        return $user->canDeleteMaterial();
    }

    /**
     * Determine whether the user can restore the Material.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Material  $material
     * @return mixed
     */
    public function restore(Admin $user, Material $material)
    {
        $user->canCreateMaterial();
    }

    /**
     * Determine whether the user can permanently delete the Material.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Material  $material
     * @return mixed
     */
    public function forceDelete(Admin $user, Material $material)
    {
        $user->canDeleteMaterial();
    }
}
