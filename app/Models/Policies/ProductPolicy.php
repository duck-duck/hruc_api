<?php

namespace App\Models\Policies;

use App\Models\Product;
use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any Product.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        return $user->canReadProduct();
    }

    /**
     * Determine whether the user can view the Product.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function view(Admin $user, Product $product)
    {
        return $user->canReadProduct();
    }

    /**
     * Determine whether the user can create Product.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->canCreateProduct();
    }

    /**
     * Determine whether the user can update the Product.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function update(Admin $user, Product $product)
    {
        return $user->canUpdateProduct();
    }

    /**
     * Determine whether the user can delete the Product.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function delete(Admin $user, Product $product)
    {
        return $user->canDeleteProduct();
    }

    /**
     * Determine whether the user can restore the Product.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function restore(Admin $user, Product $product)
    {
        $user->canCreateProduct();
    }

    /**
     * Determine whether the user can permanently delete the Product.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Product  $product
     * @return mixed
     */
    public function forceDelete(Admin $user, Product $product)
    {
        $user->canDeleteProduct();
    }
}
