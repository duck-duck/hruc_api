<?php

namespace App\Models\Policies;

use App\Models\Camera;
use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CameraPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any cameras.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function viewAny(Admin $user)
    {
        return $user->canReadCamera();
    }

    /**
     * Determine whether the user can view the camera.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Camera  $camera
     * @return mixed
     */
    public function view(Admin $user, Camera $camera)
    {
        return $user->canReadCamera();
    }

    /**
     * Determine whether the user can create cameras.
     *
     * @param  \App\Models\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->canCreateCamera();
    }

    /**
     * Determine whether the user can update the camera.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Camera  $camera
     * @return mixed
     */
    public function update(Admin $user, Camera $camera)
    {
        return $user->canUpdateCamera();
    }

    /**
     * Determine whether the user can delete the camera.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Camera  $camera
     * @return mixed
     */
    public function delete(Admin $user, Camera $camera)
    {
        return $user->canDeleteCamera();
    }

    /**
     * Determine whether the user can restore the camera.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Camera  $camera
     * @return mixed
     */
    public function restore(Admin $user, Camera $camera)
    {
        $user->canCreateCamera();
    }

    /**
     * Determine whether the user can permanently delete the camera.
     *
     * @param  \App\Models\Admin  $user
     * @param  \App\Models\Camera  $camera
     * @return mixed
     */
    public function forceDelete(Admin $user, Camera $camera)
    {
        $user->canCreateCamera();
    }
}
