<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasTranslations;

    public $translatable = ['name','description'];

    protected $fillable = [
        'order',
        'image'
    ];

    /**
     * Get all of the post's comments.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
