<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;

    private $custom_construct_fields = [
        'category_id',
        'camera_id',
        'lense_id',
        'material_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'custom_construct' => 'array',
    ];

    protected $fillable = [
        'price'
    ];

    /**
     * The order that the item belongs.
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * The product that the item belongs.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * Set data to custom_construct
     * @param array $data
     */
    public function setCustomConstruct($data) : void
    {
        $custom_construct = [];
        foreach ($this->custom_construct_fields as $field) {
            $custom_construct[$field] = empty($data[$field]) ?: $data[$field];
        }
        $this->custom_construct = $custom_construct;
        $this->save();
    }
}
