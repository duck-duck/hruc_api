<?php

namespace App\Models;

use App\Scopes\AdminScope;

class Admin extends User
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AdminScope);
    }
}
