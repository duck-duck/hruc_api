<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    /**
     * Get the owning commentable model.
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
