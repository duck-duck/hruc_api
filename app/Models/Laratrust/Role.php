<?php

namespace App\Models\Laratrust;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    const ADMIN = 'admin';
    const MANAGER = 'manager';
    const CLIENT = 'client';
}
