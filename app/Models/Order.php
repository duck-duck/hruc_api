<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use EloquentFilter\Filterable;

class Order extends Model
{
    use SoftDeletes, Uuid, Filterable;

    protected $fillable = [
        'email',
        'phone',
        'total',
        'comment',
        'ip',
        'accept_language'
    ];

    /**
     * The status that the order belongs.
     */
    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    /**
     * The user that the order belongs.
     */
    public function user()
    {
        return $this->belongsTo(Client::class,'user_id');
    }

    /**
     * Get all of the order's items.
     */
    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }
}
