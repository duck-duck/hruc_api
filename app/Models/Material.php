<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;
use Illuminate\Support\Facades\Storage;

class Material extends Model
{
    use Filterable;

    protected $fillable = [
        'name'
    ];

    public function getImageFullUrl()
    {
        if (!$this->image) {
            return asset('default_img.png');
        }

        return url('storage/'.$this->getImagePath().'/'.$this->image);
    }

    public function getImagePath()
    {
        return strtolower(class_basename($this));
    }

    /**
     * The products that belong to the material.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class,'product_material');
    }
}
