<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Notifications\Client\VerifyEmail;
use App\Scopes\ClientScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use App\Notifications\Client\ResetPassword as ResetPasswordNotification;

class Client extends User implements MustVerifyEmail, CanResetPassword
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ClientScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone'
    ];

    /**
     * Get all of the user's orders.
     */
    public function orders()
    {
        return $this->hasMany(Order::class,'user_id');
    }

    public function setPassword($password)
    {
        $this->password = $password;
        $this->save();
    }

    public function setEmail($email)
    {
        $this->email = $email;
        $this->save();
    }

    public function setConfirmationCode()
    {
        $this->confirmation_code = Str::random(4);
        $this->save();
    }

    public function destroyConfirmationCode()
    {
        $this->confirmation_code = null;
        $this->save();
    }

    /**
     * Scope a query to only include user match by confirmation code.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereConfirmationCode($query, $code)
    {
        return $query->where('confirmation_code', $code)
            ->whereNotNull('confirmation_code');
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify((new VerifyEmail)->onQueue('email'));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
