<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Traits\Controller\GuardTrait;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, GuardTrait;

    /**
     * Sets the guard to be used during authentication.
     *
     * @var string
     */
    protected $guard = 'admin';

    protected $per_page = 10;

    public function getPerPage()
    {
        return request()->per_page ? request()->per_page : $this->per_page;
    }
}
