<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Services\Admin\OrderService;
use \Illuminate\Http\JsonResponse;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrdersResource;
use Spatie\QueryBuilder\QueryBuilder;
use App\Http\Requests\Order\OrderUpdate;

class OrderController extends Controller
{

    protected $query_fields = ['phone', 'email','status'];

    /**
     *
     * @var ProductService
     */
    private $productService;

    public function __construct()
    {
        parent::__construct();
        $this->orderService = new OrderService();
        $this->authorizeResource(Order::class, 'order');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) : OrdersResource
    {
        $filter = Order::filter($request->all());
        $orders = QueryBuilder::for($filter)
            ->allowedSorts($this->query_fields)
            ->latest()
            ->with('items')
            ->paginate($this->getPerPage());
        return new OrdersResource($orders);
    }

    /**
     * Display status list.
     *
     * @return \Illuminate\Http\Response
     */
    public function statusList()
    {
        return OrderStatus::all(['id','name'])->pluck('name','id');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return OrderResource
     */
    public function show(Order $order) : OrderResource
    {
        return new OrderResource($order);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  OrderUpdate  $request
     * @param  \App\Models\Order  $order
     * @return OrderResource
     */
    public function update(OrderUpdate $request, Order $order) : OrderResource
    {
        $order = $this->orderService->update($order, $request->all());

        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderUpdate  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(OrderUpdate $request, Order $order) : JsonResponse
    {
        $this->orderService->update($order, $request->all());

        return response()->json(['status'=>true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOrderItem(Order $order, OrderItem $orderItem) : JsonResponse
    {
        $result = $this->orderService->deleteOrderItem($orderItem);

        return response()->json(['status'=>$result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Order $order) : JsonResponse
    {
        $result = $this->orderService->delete($order);

        return response()->json(['status'=>$result]);
    }
}
