<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Resources\{MaterialResource, MaterialsResource};
use App\Http\Requests\Material\{MaterialCreate, MaterialUpdate};
use App\Services\Admin\MaterialService;
use App\Models\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    protected $query_fields = ['name'];

    /**
     *
     * @var MaterialService
     */
    private $materialService;

    public function __construct()
    {
        parent::__construct();
        $this->materialService = new MaterialService();
        $this->authorizeResource(Material::class, 'material');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new MaterialsResource(Material::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MaterialCreate  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialCreate $request) : MaterialResource
    {
        $material = $this->materialService->create($request);

        return new MaterialResource($material);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Material $material
     * @return LensResource
     */
    public function show(Material $material) : MaterialResource
    {
        return new MaterialResource($material);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  MaterialUpdate $request
     * @param  \App\Models\Material  $material
     * @return LensResource
     */
    public function update(MaterialUpdate $request, Material $material) : MaterialResource
    {
        $material = $this->materialService->update($material, $request);

        return new MaterialResource($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        $result = $this->materialService->delete($material);
        return response()->json(['status'=>$result]);
    }

    public function removeImage(Material $material)
    {
        $result = $this->materialService->removeImage($material);
        return response()->json(['status'=>$result]);
    }
}
