<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Resources\{CameraResource,CamerasResource};
use App\Http\Requests\Camera\{CameraCreate, CameraUpdate};
use Spatie\QueryBuilder\QueryBuilder;
use App\Services\Admin\CemeraService;
use App\Models\Camera;
use Illuminate\Http\Request;

class CameraController extends Controller
{
    protected $query_fields = ['name', 'width', 'height', 'depth'];

    /**
     *
     * @var CemeraService
     */
    private $cameraService;


    public function __construct()
    {
        parent::__construct();
        $this->cameraService = new CemeraService();
        $this->authorizeResource(Camera::class, 'camera');
    }

    /**
     * Display a listing of the resource.
     * @param Illuminate\Http\Request $request
     * @return CamerasResource
     */
    public function index(Request $request) : CamerasResource
    {
        $filter = Camera::filter($request->all());
        $cameras = QueryBuilder::for($filter)
            ->allowedSorts($this->query_fields)
            ->latest()
            ->with('brand')
            ->paginate($this->getPerPage());
        return new CamerasResource($cameras);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CameraCreate  $request
     * @return CameraResource
     */
    public function store(CameraCreate $request) : CameraResource
    {
        $camera = $this->cameraService->create($request->all());
        return new CameraResource($camera);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Camera  $camera
     * @return CameraResource
     */
    public function show(Camera $camera) : CameraResource
    {
        return new CameraResource($camera);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CameraUpdate  $request
     * @param  \App\Models\Camera  $camera
     * @return CameraResource
     */
    public function update(CameraUpdate $request, Camera $camera) : CameraResource
    {
        $camera = $this->cameraService->update($camera, $request->all());
        return new CameraResource($camera);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function destroy(Camera $camera)
    {
        $result = $this->cameraService->delete($camera);
        return response()->json(['status'=>$result]);
    }
}
