<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Services\Admin\LensService;
use Spatie\QueryBuilder\QueryBuilder;
use App\Http\Requests\Lense\{LensCreate, LensUpdate};
use App\Http\Resources\{LensResource, LenseResource};
use App\Models\Lense;
use Illuminate\Http\Request;

class LensController extends Controller
{
    protected $query_fields = ['name', 'width', 'depth'];

    /**
     *
     * @var LensService
     */
    private $lensService;


    public function __construct()
    {
        parent::__construct();
        $this->lensService = new LensService();
        $this->authorizeResource(Lense::class, 'lense');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) : LenseResource
    {
        $filter = Lense::filter($request->all());
        $lense = QueryBuilder::for($filter)
            ->allowedSorts($this->query_fields)
            ->latest()
            ->with('brand')
            ->paginate($this->getPerPage());
        return new LenseResource($lense);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  LensCreate  $request
     * @return LensResource
     */
    public function store(LensCreate $request) : LensResource
    {
        $lense = $this->lensService->create($request->all());
        return new LensResource($lense);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lense $lense
     * @return LensResource
     */
    public function show(Lense $lense) : LensResource
    {
        return new LensResource($lense);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  LensUpdate $request
     * @param  \App\Models\Lense  $lense
     * @return LensResource
     */
    public function update(LensUpdate $request, Lense $lense) : LensResource
    {
        $lense = $this->lensService->update($lense, $request->all());
        return new LensResource($lense);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lense  $lense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lense $lense)
    {
        $result = $this->lensService->delete($lense);
        return response()->json(['status'=>$result]);
    }
}
