<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Http\Resources\{ProductResource, ProductsResource};
use App\Http\Requests\Product\{ProductCreate, ProductUpdate};
use App\Http\Requests\Product\{UploadImage, UploadImages};
use App\Services\Admin\ProductService;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Image;

class ProductController extends Controller
{
    /**
     *
     * @var ProductService
     */
    private $productService;

    public function __construct()
    {
        parent::__construct();
        $this->productService = new ProductService();
        $this->authorizeResource(Product::class, 'product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return ProductsResource
     */
    public function index() : ProductsResource
    {
        $products = Product::active()->paginate($this->getPerPage());

        return new ProductsResource($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreate $request)
    {
        $product = $this->productService->create($request->all());

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return ProductResource
     */
    public function show(Product $product) : ProductResource
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductUpdate  $request
     * @param  \App\Models\Product  $product
     * @return ProductResource
     */
    public function update(ProductUpdate $request, Product $product) : ProductResource
    {
        $product = $this->productService->update($product, $request->all());

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $result = $this->productService->delete($product);
        return response()->json(['status'=>$result]);
    }

    public function uploadImage(UploadImage $request)
    {
        $image_name = $this->productService->uploadImage($request->image);

        return response()->json(['image_name'=>$image_name]);
    }

    public function uploadImages(UploadImages $request)
    {
        $images = [];
        foreach ($request->images as $image) {
            $images[] = $this->productService->uploadImage($image);
        }

        return response()->json(['images'=>$images]);
    }

    public function removeImage(Product $product, Image $image)
    {
        $result = $this->productService->removeImage($product, $image);
        return response()->json(['status'=>$result]);
    }
}
