<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Client\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use App\Http\Requests\Client\VerifyEmail;
use Illuminate\Http\JsonResponse;
use App\Models\Client;
use Lang;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:6,1')->only('verifyEmail', 'resend');
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  VerifyEmail $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verifyEmail(VerifyEmail $request)
    {
        $user = Client::whereConfirmationCode($request->confirmation_code)
            ->where('email',$request->email)->first();

        if (!$user) {
            abort(403, Lang::get('auth.failed'));
        }

        $token = $this->guard()->login($user);
        $user->destroyConfirmationCode();

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return $this->respondWithToken($token);
    }


    /**
     * Resend the email verification notification.
     *
     * @param  Client $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(Client $client)
    {
        if ($client->hasVerifiedEmail()) {
            return response()->json(['message'=>Lang::get('auth.email_has_verified')]);
        }

        $client->sendEmailVerificationNotification();

        return response()->json(['message'=>Lang::get('auth.verification_email_was_sent')]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function respondWithToken($token) : JsonResponse
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expired'   => auth()->factory()->getTTL() * 60
        ]);
    }
}
