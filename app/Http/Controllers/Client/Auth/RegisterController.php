<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Client\Controller;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Client\EmailRegistration;
use Illuminate\Http\JsonResponse;
use App\Services\Client\UserService;
use App\Http\Resources\ClientResource;

class RegisterController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  EmailRegistration  $request
     * @return ClientResource
     */
    public function registerByEmail(EmailRegistration $request) : ClientResource
    {
        $user = $this->userService->registerByEmail($request->all());

        event(new Registered($user));

        return new ClientResource($user);
    }

}
