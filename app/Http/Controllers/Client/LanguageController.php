<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LanguageController extends Controller
{
    /**
    * Set locale cookie (e.g. en|fi)
    * @param Request $request
    * @return void
    **/
    public function change(Request $request)
    {
        $cookie = cookie('locale', $request->locale, 10080, null, env('COOKIE_DOMAIN'));// 1 week

        return response()->json(['locale'=>$request->locale])->cookie($cookie);
    }
}
