<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Camera;
use App\Models\Material;
use App\Models\Lense;
use App\Models\Category;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id'    => $this->id,
            'item'  => $this->getItemInfo(),
        ];
    }

    /**
     * Get order's item info
     * @return array $items
     */
    protected function getItemInfo()
    {
        $data['price']=$this->price;
        $data['description'] = '';

        if (!empty($this->custom_construct)) {
            $data['description'] = Category::find($this->custom_construct['category_id'])->name."\n";

            if ($this->custom_construct['camera_id']) {
                $camera = Camera::with('brand')->find($this->custom_construct['camera_id']);
                $data['description'] .= $camera->brand->name.' '.$camera->name."\n";
            }

            if ($this->custom_construct['lense_id']) {
                $lense = Lense::with('brand')->find($this->custom_construct['lense_id']);
                $data['description'] .= $lense->brand->name.' '.$lense->name."\n";
            }

            if ($this->custom_construct['material_id']) {
                $material = Material::find($this->custom_construct['material_id']);
                $data['description'] .= $material->name."\n";
            }
        } else {
            $data['description'] = $this->product->name;
        }

        return $data;
    }
}
