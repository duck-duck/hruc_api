<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'image' => $this->image,
            'image_url' => $this->getImageFullUrl(),
        ];
    }
}
