<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Laratrust\Role;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'is_admin'=>$this->hasRole(Role::ADMIN),
            'is_manager'=>$this->hasRole(Role::MANAGER),
            'name'  => $this->name,
            'email'  => $this->email,
        ];
    }
}
