<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CameraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'brand_id'  => $this->brand->id,
            'width' => $this->width,
            'height'=> $this->height,
            'depth' => $this->depth,
            'brand' => new BrandResource($this->brand),
        ];
    }
}
