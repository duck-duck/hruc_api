<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id'    => $this->id,
            'email'  => $this->email,
            'phone'  => $this->phone,
            'comment'  => $this->comment,
            'status_id'  => $this->status_id,
            'total'  => $this->total,
            'ip'  => $this->ip,
            'accept_language'  => $this->accept_language,
            'created_at'=>$this->created_at->toDateTimeString(),
            'items' => new OrderItemsResource($this->items)
        ];
    }
}
