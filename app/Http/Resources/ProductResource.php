<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        self::withoutWrapping();
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'status'    => $this->status,
            'slug'      => $this->slug,
            'price'     => $this->price,
            'category'  => $this->getCategory(),
            'quantity'  => $this->quantity,
            'image'     => $this->image ? $this->getImagePath($this->image, true) : null,
            'created_at'=> $this->created_at->toDateTimeString(),
            'description'=>$this->getTranslations()['description'],
            'cameras'   => $this->cameras ? $this->cameras->pluck('name','id') : [],
            'lenses'    => $this->lenses ? $this->lenses->pluck('name','id') : [],
            'materials' => $this->getMaterials(),
            'images'    => $this->getImages(),
        ];
    }

    /**
     * @return array $images
     */
    protected function getImages() : array
    {
        $images = [];
        foreach ($this->images as $key => $image) {
            $images[$key]['id'] = $image->id;
            $images[$key]['name'] = $image->name;
            $images[$key]['url'] = $this->getImagePath($image->name, true);
        }

        return $images;
    }

    /**
     * @return array $materials
     */
    protected function getMaterials() : array
    {
        $materials = [];
        foreach ($this->materials as $key => $material) {
            $materials[$key]['id'] = $material->id;
            $materials[$key]['name'] = $material->name;
            $materials[$key]['image_url'] = $material->getImageFullUrl();
        }

        return $materials;
    }

    protected function getCategory()
    {
        return [
            'id'=>$this->category->id,
            'name'=>$this->category->name,
        ];
    }
}
