<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Illuminate\Support\Facades\Cookie;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Cookie::get('locale');

        if (!$locale) {
            $locale = substr($request->header('accept-language'), 0, 2); //xx
        }

        App::setLocale($locale);

        return $next($request);
    }
}
