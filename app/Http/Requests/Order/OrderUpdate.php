<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id'  => 'required|exists:order_statuses,id',
            'email'=>'sometimes|required|email',
            'phone'=>'sometimes|required|string',
            'total'=>'sometimes|required|numeric',
            'comment'=>'sometimes|string',
        ];
    }
}
