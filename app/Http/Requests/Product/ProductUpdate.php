<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|string|min:3|unique:products,name,'.$this->product->id,
            'slug' => 'required|string|min:3|unique:products,slug,'.$this->product->id,
            'status'=>'required|boolean',
            'quantity'=>'required|integer',
            'price'=>'required|integer',
            'category_id'  => 'required|exists:categories,id',
            'description'=>'required|array',
            'description.en'=>'required|string',
            'description.ru'=>'required|string',
            'description.uk'=>'required|string',
            'image'=>'required|string',
            'images'=>'nullable|array',
            'cameras'=>'nullable|array|exists:cameras,id',
            'lenses'=>'nullable|array|exists:lenses,id',
            'materials'=>'nullable|array|exists:materials,id',
        ];
    }
}
