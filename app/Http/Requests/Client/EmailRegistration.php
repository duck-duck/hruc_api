<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;
use LVR\Phone\Phone;

class EmailRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required', 'string', 'min:2', 'max:100'],
            'email'     => ['required', 'string', 'email', 'max:100', 'unique:users'],
            'phone'     => ['required', new Phone, 'min:11','max:15'],
            'password'  => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }
}
