<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class CameraFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function name($value)
    {
        $this->where('name', 'like', '%'.$value.'%');
    }

    public function brand($brand_id)
    {
        $this->whereHas('brand', function($brand) use($brand_id){
            $brand->where('id', $brand_id);
        });
    }

    public function width($value)
    {
        $this->where('width', $value);
    }

    public function height($value)
    {
        $this->where('height', $value);
    }

    public function depth($value)
    {
        $this->where('depth', $value);
    }
}
