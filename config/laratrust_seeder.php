<?php

return [
    'role_structure' => [
        'admin' => [
            'user' => 'c,r,u,d',
            'brand' => 'c,r,u,d',
            'camera' => 'c,r,u,d',
            'lens' => 'c,r,u,d',
            'material' => 'c,r,u,d',
            'cloth' => 'c,r,u,d',
            'order' => 'r,u,d',
            'product' => 'c,r,u,d',
        ],
        'manager' => [
            'user' => 'c,r,u',
            'brand' => 'c,r,u',
            'camera' => 'c,r,u',
            'lens' => 'c,r,u',
            'material' => 'c,r,u',
            'cloth' => 'c,r,u',
            'order' => 'r,u',
            'product' => 'c,r,u',
        ],
        'client' => [
            'order' => 'c,r'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
