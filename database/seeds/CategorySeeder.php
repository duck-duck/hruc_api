<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'=>
                    [
                        'uk' => 'Чохли до фотокамер',
                        'ru' => 'Чехлы для камер',
                        'en' => 'Digital camera cases'
                    ],
                'description'=>
                    [
                        'uk' => 'Шось там про чохли для камер',
                        'ru' => 'Чудесные чехлы для камер',
                        'en' => 'Description Digital camera cases'
                    ],
                'order'=>1
            ],
            [
                'name'=>
                    [
                        'uk' => 'Чохли до об\'єктивів',
                        'ru' => 'Чехлы для объективов',
                        'en' => 'Lens cases'
                    ],
                'description'=>
                    [
                        'uk' => 'Чохли до об\'єктивів опис',
                        'ru' => 'Чехлы для объективов описание',
                        'en' => 'Lens cases description'
                    ],
                'order'=>2
            ],
            [
                'name'=>
                    [
                        'uk' => 'Ремені для фотокамер',
                        'ru' => 'Ремни для фотокамер',
                        'en' => 'Belts'
                    ],
                'description'=>
                    [
                        'uk' => 'Ремені для фотокамер опис',
                        'ru' => 'Ремни для фотокамер описание',
                        'en' => 'Belts description'
                    ],
                'order'=>3
            ]
        ];

        Schema::disableForeignKeyConstraints();
        Category::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($categories as $category) {
            $model = Category::create($category);
        }
    }
}
