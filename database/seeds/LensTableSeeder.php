<?php

use Illuminate\Database\Seeder;
use App\Models\Lense;
use App\Models\Brand;

class LensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            'nikon' => [
                ['name' => 'AF-S NIKKOR 24-85mm f/3,5-4,5G ED VR','width'=>78,'depth'=>82],
                ['name' => 'AF-S NIKKOR 24-85mm f/3.5-4.5G ED-IF','width'=>73,'depth'=>73],
                ['name' => 'AF-P DX NIKKOR 18-55mm f/3.5-5.6G','width'=>65,'depth'=>63],
                ['name' => 'AF-S DX NIKKOR 18-55mm f/3.5-5.6G VR','width'=>73,'depth'=>80],
                ['name' => 'AF-S DX NIKKOR 18-55mm f/3.5-5.6G VR II','width'=>66,'depth'=>60],
                ['name' => 'AF-S DX Zoom-Nikkor 18-55mm f/3.5-5.6G ED II','width'=>71,'depth'=>74],
                ['name' => 'AF-P DX NIKKOR 18-55mm f/3.5-5.6G VR','width'=>65,'depth'=>62],
                ['name' => 'AF-S NIKKOR 50mm f/1.4G','width'=>74,'depth'=>54],
                ['name' => 'AF-S DX NIKKOR 16-85mm f/3.5-5.6G ED VR','width'=>72,'depth'=>85],
                ['name' => 'AF-S DX NIKKOR 35mm f/1.8G','width'=>70,'depth'=>53],
                ['name' => 'AF-S NIKKOR 85mm f/1.4G','width'=>87,'depth'=>84],
                ['name' => 'AF-S NIKKOR 50 мм f/1,8G','width'=>72,'depth'=>53],
                ['name' => 'AF-S NIKKOR 85 мм f/1,8G','width'=>80,'depth'=>73],
                ['name' => 'AF-S DX NIKKOR 16-80mm f/2.8-4E ED VR','width'=>80,'depth'=>86],
                ['name' => 'AF-S DX NIKKOR 18-105mm f/3.5-5.6G ED VR','width'=>76,'depth'=>89],
                ['name' => 'Nikkor AF DX 18-135/3.5-5.6 G IF-ED','width'=>74,'depth'=>87],
                ['name' => 'Nikkor 18-70mm f3.5-4.5G ED-IF AF DX','width'=>73,'depth'=>76],
                ['name' => 'Nikon 18-140mm f/3.5-5.6G ED VR AF-S DX Nikkor','width'=>78,'depth'=>97],
            ],
            'canon' => [
                ['name' => 'Canon EF-S 18-55mm f/4-5.6','width'=>67,'depth'=>62],
                ['name' => 'Canon EF 28-105mm F3.5-4.5 USM','width'=>72,'depth'=>75],
                // ['name' => '','width'=>,'depth'=>]
            ],
            'pentax' => [
                ['name' => 'SMC Pentax-DAL 1:3.5-5.6 18-55mm','width'=>68,'depth'=>69],
                ['name' => 'SMC PENTAX DA 18-135mm f/3.5-5.6','width'=>73,'depth'=>76]
            ]
        ];

        Schema::disableForeignKeyConstraints();
        Lense::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($brands as $brand => $lenses) {
            $brand = Brand::where('name',$brand)->first();
            foreach ($lenses as $lense) {
                $model = Lense::create($lense);
                $model->brand()->associate($brand)->save();
            }
        }
    }
}
