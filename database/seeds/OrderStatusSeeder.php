<?php

use Illuminate\Database\Seeder;
use App\Models\OrderStatus;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name'=>
                    [
                        'uk' => 'Новий заказ',
                        'ru' => 'Новый заказ',
                        'en' => 'New order'
                    ]
            ],
            [
                'name'=>
                    [
                        'uk' => 'В обробці',
                        'ru' => 'В обработке',
                        'en' => 'In progress'
                    ]
            ],
            [
                'name'=>
                    [
                        'uk' => 'Опрацьований',
                        'ru' => 'Обработан',
                        'en' => 'Done'
                    ]
            ],
            [
                'name'=>
                    [
                        'uk' => 'Повернуто',
                        'ru' => 'Возврат',
                        'en' => 'Returned'
                    ]
            ],
            [
                'name'=>
                    [
                        'uk' => 'Відмінений',
                        'ru' => 'Отменён',
                        'en' => 'Canceled'
                    ]
            ]
        ];

        Schema::disableForeignKeyConstraints();
        OrderStatus::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($statuses as $status) {
            $model = OrderStatus::create($status);
        }
    }
}
