<?php

use Illuminate\Database\Seeder;
use App\Models\Material;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materials = [
            ['name' => 'Material1'],
            ['name' => 'Material2'],
            ['name' => 'Material3']
        ];

        Schema::disableForeignKeyConstraints();
        Material::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($materials as $material) {
            $model = Material::create($material);
        }
    }
}
