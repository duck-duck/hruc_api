<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BrandsTableSeeder::class);
        $this->call(CamerasTableSeeder::class);
        $this->call(LensTableSeeder::class);
        $this->call(MaterialsTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(OrderStatusSeeder::class);

        if (app()->environment('local')) {
            $this->call(LaratrustSeeder::class);
            $this->call(ProductSeeder::class);
            $this->call(OrderSeeder::class);
        }
    }
}
