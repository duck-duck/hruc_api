<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Camera;
use App\Models\Material;
use App\Models\Lense;
use App\Models\Category;
use App\Models\Image as ModelImage;
use Illuminate\Support\Facades\Storage;
use App\Services\Admin\ProductService;
use Faker\Factory as Faker;
use Faker\Generator;
use Faker\Provider\en_US\Text as EnText;
use Faker\Provider\ru_RU\Text as RuText;
use Faker\Provider\uk_UA\Text as UkText;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateProductTable();

        $faker = Faker::create('en_US');
        $productService = new ProductService();
        $cameras = Camera::all();
        $materials = Material::all();
        $lenses = Lense::all();
        $categories = Category::all();
        $storagePath = Storage::disk()->getDriver()->getAdapter()->getPathPrefix().(new Product)->getStoragePath();
        exec('rm -r ' . $storagePath.'/*');

        $ukFaker = new Generator();
        $ukFaker->addProvider(new UkText($ukFaker));
        $ruFaker = new Generator();
        $ruFaker->addProvider(new RuText($ruFaker));
        for ($i=0; $i < 5; $i++) {
            $data = [
                'name'          =>  $faker->unique()->sentence($nbWords = 2),
                'slug'          =>  $faker->unique()->slug,
                'price'         =>  $faker->numberBetween($min = 400, $max = 500),
                'status'        =>  1,
                'quantity'      =>  $faker->randomDigit,
                'category_id'   =>  $categories->random(1)->first()->id,
                'description'   =>  [
                        'uk'    =>  $ukFaker->realText($maxNbChars = 200),
                        'en'    =>  $faker->realText($maxNbChars = 200),
                        'ru'    =>  $ruFaker->realText($maxNbChars = 200),
                ],
                'image' =>  $this->storeFile($faker, $storagePath)
            ];

            $product = Product::create($data);
            $product->category()->associate($data['category_id'])->save();
            $productService->syncImages($product, [
                $this->storeFile($faker, $storagePath),
                $this->storeFile($faker, $storagePath)
            ]);
            $product->materials()->sync([
                $materials->first()->id,
                $materials->last()->id
            ]);
            $product->cameras()->sync([
                $cameras->random(1)->first()->id,
                $cameras->random(1)->first()->id
            ]);
            $product->lenses()->sync([
                $lenses->random(1)->first()->id,
                $lenses->random(1)->first()->id
            ]);
        }
    }

    /**
     * Copy a random file from the source to the target directory
     * and returns the fullpath or filename
     *
     * @param string $storagePath
     * @return string fullpath|filename
     */
    private function storeFile($faker, $storagePath)
    {
        return $faker->file(resource_path('fake-images'), $storagePath, false);
    }

    /**
     * Truncates product table
     *
     * @return    void
     */
    public function truncateProductTable()
    {
        Schema::disableForeignKeyConstraints();
        ModelImage::where('imageable_type','product')->delete();
        Product::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
