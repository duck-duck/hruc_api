<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        $brands = [
            ['name' => 'Nikon','created_at' => $now,'updated_at' => $now],
            ['name' => 'Canon','created_at' => $now,'updated_at' => $now],
            ['name' => 'Pentax','created_at' => $now,'updated_at' => $now]
        ];

        Schema::disableForeignKeyConstraints();
        Brand::truncate();
        Schema::enableForeignKeyConstraints();

        Brand::insert($brands);
    }
}
