<?php

use Illuminate\Database\Seeder;
use App\Models\Camera;
use App\Models\Brand;

class CamerasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            'nikon' => [
                // small
                ['name' => 'D3000','width'=>126,'height'=>94,'depth'=>64],
                ['name' => 'D3100','width'=>124,'height'=>96,'depth'=>75],
                ['name' => 'D3200','width'=>125,'height'=>96,'depth'=>77],
                ['name' => 'D3300','width'=>124,'height'=>98,'depth'=>76],
                ['name' => 'D5000','width'=>127,'height'=>104,'depth'=>80],
                ['name' => 'D5100','width'=>128,'height'=>97,'depth'=>79],
                ['name' => 'D5200','width'=>129,'height'=>98,'depth'=>78],
                ['name' => 'D5300','width'=>125,'height'=>98,'depth'=>76],
                ['name' => 'D5500','width'=>129,'height'=>98,'depth'=>78],
                ['name' => 'D5600','width'=>124,'height'=>97,'depth'=>70],
                //big
                ['name' => 'D70','width'=>140,'height'=>111,'depth'=>78],
                ['name' => 'D80','width'=>132,'height'=>103,'depth'=>77],
                ['name' => 'D90','width'=>132,'height'=>103,'depth'=>77],
                ['name' => 'D7000','width'=>132,'height'=>105,'depth'=>77],
                ['name' => 'D7100','width'=>136,'height'=>107,'depth'=>76],
                ['name' => 'D7200','width'=>136,'height'=>107,'depth'=>76],
                ['name' => 'D7500','width'=>136,'height'=>104,'depth'=>73]
            ],
            'canon' => [
                // small
                ['name' => '450D','width'=>129,'height'=>98,'depth'=>62],
                ['name' => '500D','width'=>129,'height'=>98,'depth'=>62],
                ['name' => '550D','width'=>129,'height'=>98,'depth'=>62],
                ['name' => '600D','width'=>133,'height'=>100,'depth'=>80],
                ['name' => '650D','width'=>133,'height'=>100,'depth'=>79],
                ['name' => '700D','width'=>133,'height'=>100,'depth'=>79],
                ['name' => '750D','width'=>132,'height'=>101,'depth'=>78],
                ['name' => '760D','width'=>132,'height'=>101,'depth'=>78],
                ['name' => '70D','width'=>139,'height'=>104,'depth'=>79],
                ['name' => '800D','width'=>131,'height'=>100,'depth'=>76],
                ['name' => '1000D','width'=>126,'height'=>98,'depth'=>62],
                ['name' => '1100D','width'=>130,'height'=>100,'depth'=>78],
                ['name' => '1300D','width'=>129,'height'=>101,'depth'=>78],
                //big
                ['name' => '30D','width'=>144,'height'=>106,'depth'=>74],
                ['name' => '40D','width'=>146,'height'=>108,'depth'=>74],
                ['name' => '50D','width'=>146,'height'=>108,'depth'=>74],
                ['name' => '60D','width'=>145,'height'=>106,'depth'=>79],
                ['name' => '70D','width'=>139,'height'=>105,'depth'=>79],
                ['name' => '80D','width'=>139,'height'=>106,'depth'=>79]
            ],
            'pentax' => [
                ['name' => 'Kr','width'=>125,'height'=>97,'depth'=>68],
                ['name' => 'К3','width'=>132,'height'=>103,'depth'=>78],
                ['name' => 'К5','width'=>131,'height'=>97,'depth'=>73],
                ['name' => 'К30','width'=>130,'height'=>97,'depth'=>72],
                ['name' => 'К50','width'=>130,'height'=>97,'depth'=>71],
                ['name' => '645D','width'=>156,'height'=>117,'depth'=>119]
            ]
        ];

        Schema::disableForeignKeyConstraints();
        Camera::truncate();
        Schema::enableForeignKeyConstraints();

        foreach ($brands as $brand => $cameras) {
            $brand = Brand::where('name',$brand)->first();
            foreach ($cameras as $camera) {
                $model = Camera::create($camera);
                $model->brand()->associate($brand)->save();
            }
        }

    }
}
