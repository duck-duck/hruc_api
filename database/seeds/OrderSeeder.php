<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Faker\Generator;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Client;
use App\Models\Camera;
use App\Models\Material;
use App\Models\Lense;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateOrderTable();

        $faker = Faker::create();
        $products = Product::all();
        $client = Client::first();
        $orderSatus = OrderStatus::all();
        $locales = config('app.locales');

        for ($i=0; $i < 5; $i++) {
            // Create order
            $data = [
                'status_id'         => $orderSatus->random(1)->first()->id,
                'email'             => $faker->email,
                'phone'             => $faker->e164PhoneNumber,
                'total'             => $faker->numberBetween(500, 1000),
                'comment'           => $faker->sentence(15),
                'ip'                => $faker->ipv4,
                'accept_language'   => $locales[array_rand($locales)]
            ];
            $order = $client->orders()->create($data);
            $order->status()->associate($data['status_id'])->save();

            //Add order's items
            $items = [];
            $order_total = 0;
            foreach ($products->random(2) as $key=>$product) {
                $items[$key]['product_id'] = $product->id;
                $items[$key]['price'] = $product->price;
                $order_total += $product->price;
            }
            $items[] = $randomProduct = $this->randomCustomProduct($faker);
            $order_total += $randomProduct['price'];
            $order->total = $order_total;
            $order->save();
            foreach ($items as $item) {
                $newItem = $order->items()->create($item);
                if (!empty($item['product_id'])) {
                    $newItem->product()->associate($item['product_id'])->save();
                }

                if (!empty($item['custom_construct'])) {
                    $newItem->setCustomConstruct($item['custom_construct']);
                }
            }
        }
    }

    /**
     * Build rundom custom product
     * @param  Generator $faker
     * @return array $custom
     */
    private function randomCustomProduct(Generator $faker) : array
    {
        $category = Category::all()->random(1)->first();
        $camera = Camera::all();
        $lense= Lense::all();
        $material = Material::all();
        $custom['price'] = $faker->numberBetween(400, 600);
        $custom['custom_construct']['category_id'] = $category->id;
        switch ($category->id) {
            case '1':
                $custom['custom_construct'] = [
                    'camera_id'     =>  $camera->random(1)->first()->id,
                    'lense_id'      =>  $lense->random(1)->first()->id,
                    'material_id'   =>  $material->random(1)->first()->id,
                ];
                break;
            case '2':
                $custom['custom_construct'] = [
                    'material_id'   => $material->random(1)->first()->id,
                ];
                break;
            case '3':
                $custom['custom_construct'] = [
                    'lense_id'      =>  $lense->random(1)->first()->id,
                    'material_id'   =>  $material->random(1)->first()->id,
                ];
                break;
        }

        return $custom;
    }

    /**
     * Truncates order table
     *
     * @return    void
     */
    public function truncateOrderTable()
    {
        Schema::disableForeignKeyConstraints();
        OrderItem::truncate();
        Order::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
