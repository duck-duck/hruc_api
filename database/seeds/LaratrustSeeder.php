<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Laratrust\Role;
use App\Models\Laratrust\Permission;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();

        $config = config('laratrust_seeder.role_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        foreach ($config as $role_name => $modules) {

            // Create a new role
            $role = Role::create([
                'name' => $role_name,
                'display_name' => ucwords(str_replace('_', ' ', $role_name)),
                'description' => ucwords(str_replace('_', ' ', $role_name))
            ]);
            $permissions = [];

            $this->command->info('Creating Role '. strtoupper($role_name));

            // Reading role permission modules
            foreach ($modules as $module => $value) {

                foreach (explode(',', $value) as $p => $perm) {

                    $permissionValue = $mapPermission->get($perm);

                    $permissions[] = Permission::firstOrCreate([
                        'name' => $permissionValue . '-' . $module,
                        'display_name' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                        'description' => ucfirst($permissionValue) . ' ' . ucfirst($module),
                    ])->id;

                    $this->command->info('Creating Permission to '.$permissionValue.' for '. $module);
                }
            }

            // Attach all permissions to the role
            $role->permissions()->sync($permissions);

            $this->command->info("Creating '{$role_name}' user");

            // Create default user for each role
            $model = ucwords($role_name) == 'Client' ? Client::class : Admin::class;
            $user = $model::create([
                'name' => ucwords(str_replace('_', ' ', $role_name)),
                'email' => $role_name.'@app.com',
                'password' => bcrypt('password')
            ]);

            $user->attachRole($role_name);
            $user->permissions()->sync($permissions);
        }
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        DB::table('users')->truncate();
        Role::truncate();
        Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
