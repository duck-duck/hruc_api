<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'client_verify_email' => [
        'subject'   => 'Verify Email Address',
        'line_1'  => 'Verifycation code: <b>:code</b>',
        'line_2'    => 'Thank you for using our application!'
    ],
    'email_has_verified'=>'The email has already verified',
    'verification_email_was_sent'=>'Verification email was success sent',
    'logged_out'=>'Successfully logged out',

];
